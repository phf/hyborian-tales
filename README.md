# Hyborian Tales

A Collection of Classic Sword & Sorcery Fiction
by Robert E. Howard

## Background

I wanted to read some Howard stories and looked
around online. Much to my surprise I found that
Project Gutenberg had transcribed quite a few
stories that had passed into the public domain.
Total win!

But reading Howard in plain text or ugly HTML
just didn't seem right: I wanted to read these
stories in a "properly typeset" format instead.
Years of experience with LaTeX meant I knew a
way to get exactly that, so I started converting
the plain text files to LaTeX and designed the
basic layout for a "serious" book.

Eventually it dawned on me that others might also
enjoy a properly typeset collection of Howard's
brand of Sword & Sorcery, so I started thinking
in terms of a "definite public domain edition"
instead of just a temporary crutch until I was
done reading. So I started collecting more of
the public domain stories as well as some of the
art from the original publications. It's still
not quite finished, but it's getting there.

## Goals

Here are the current goals for the project:

* Produce a properly typeset edition of all
  Robert E. Howard Sword & Sorcery stories
  available in the public domain.
* Keep to a classic layout/design, don't go
  overboard with graphical/ornamental elements.
* Include the original artwork for the stories
  as far as possible.
* Do only the minimal editing required for
  proper typesetting, do not rewrite Howard
  in any way.
* Target primarily the 6in x 9in hardcover
  format supported by lulu.com as POD.
* Support other layouts as far as possible
  without sacrificing the primary 6in x 9in
  layout; layouts suitable for a variety of
  tablets would be nice.

I am sure these will evolve, but it's roughly
what I'd like to stick to at this time.

## Internals

Here are some "rules" for the LaTeX internals:

* Avoid unicode in the source files, stick to
  traditional LaTeX commands for producing
  the "exotic" characters required by some of
  the stories.
* Define commands to take care of certain
  repetitive layout issues (we're off the mark
  a little on this right now).
* Use the appropriate dashes and quote marks
  throughout.

There's probably more but it'll evolve over
time.

## Copyright Issues

I am only using stories and art that are in the
public domain. I am crediting the sources as
well as the original artists, but this is not
meant to imply endorsement by those sources or
artists. In particular my mention of Project
Gutenberg does not imply that the compilation
itself is part of Project Gutenberg.

As for the compilation itself, the work I put into
this project, I've chosen to use a Creative Commons
Attribution-ShareAlike 3.0 Unported License. All
contributors must agree to work under that license
as well.
