# Stuff left to do

* Howard often ends quotes with --- is that appropriate?
  (see [phf] comments in source)
* Howard often has paragraphs that are not even one line
  long; seems appropriate story-wise but looks strange?
* some stories have sections but not section titles; these
  were probably set as lettrines in the original publications
  and I guess we could have two kinds of section headings,
  one of which is just the number as an inset; bigger problem
  is what to do in the running head when there's no actual
  section title

# Ideas shelved for now

* The original Weird Tales stuff is twocolumn, but I don't
  think that would look right in a book
* The original Weird Tales stuff uses dropcaps instead of
  separators, also when a section starts, but lettrine is
  (a) too manual (b) doesn't look right when a paragraph
  starts with a quote

# Resolved issues

* Q: Howard often leaves out closing quotes when a monologue
  drags on over several paragraphs; is that really the
  correct convention typographically? (see [phf] comments in source)
* A: Nathanael found https://en.wikipedia.org/wiki/Quotation_mark#Quotations_and_speech
